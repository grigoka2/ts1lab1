package cz.fel.cvut.ts1;

public class Factorial {
    public long Factorial (int n){
        int result=1,i=1;
        while(i<=n){
            result=result*i;
            i++;
        }

        return result;
    }

}
